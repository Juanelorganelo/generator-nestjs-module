/*
 * Copyright (c) 2019 Juan Manuel Garcia Junco Moreno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
const fs = require("fs");
const util = require("util");

const access = util.promisify(fs.access);

/**
 * Check if a file exists
 *
 * @param {string} file Path to check for the file
 *
 * @return {Promise<boolean>} A promise that resolves to true if the file exists
 */
async function exists(file) {
  try {
    await access(file, fs.constants.F_OK);
    return true;
  } catch (e) {
    return false;
  }
}

exports.exists = exists;
