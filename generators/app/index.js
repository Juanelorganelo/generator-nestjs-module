/*
 * Copyright (c) 2019 Juan Manuel Garcia Junco Moreno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
"use strict";
const Generator = require("yeoman-generator");
const path = require("path");
const os = require("./os");
const string = require("./string");
const NameParser = require("./name-parser");

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.name = this._getDefaultName();
    this.description = this._getDefaultDescription(this.name);
  }

  async prompting() {
    const questions = [
      {
        type: "input",
        name: "name",
        message: "Name of your NestJS module (e.g. config for ConfigModule)",
        default: this.name,
      },
      {
        type: "input",
        name: "description",
        message: "A brief description of your module",
        default: this.description,
      },
    ];
    const answers = await this.prompt(questions);

    // Transform properties
    answers.name =
      answers.name === this.name
        ? answers.name
        : NameParser.normalize(answers.name);

    // Store options for later
    this.options = answers;
  }

  writing() {
    this._writingConfig();
    this._writingLicense();
    this._writingPackage();
    this._writingEntryFiles();
    this._writingTestFiles();
  }

  async install() {
    const hasYarn = await os.executableExists("yarn");
    this.installDependencies({
      npm: !hasYarn,
      yarn: hasYarn,
      bower: false,
    });
  }

  _writingLicense() {
    this.fs.copyTpl(
      this.templatePath("LICENSE"),
      this.destinationPath("LICENSE")
    );
  }

  _writingPackage() {
    this.fs.copyTpl(
      this.templatePath("package.json"),
      this.destinationPath("package.json"),
      {
        name: `nestjs-${this.options.name}`,
        description: this.options.description,
        authorName: this.user.git.name(),
        authorEmail: this.user.git.email(),
      }
    );
  }

  _writingConfig() {
    this.fs.copyTpl(
      this.templatePath(".editorconfig"),
      this.destinationPath(".editorconfig")
    );
    this.fs.copyTpl(
      this.templatePath(".gitignore"),
      this.destinationPath(".gitignore")
    );
    this.fs.copyTpl(
      this.templatePath(".prettierrc"),
      this.destinationPath(".prettierrc")
    );
    this.fs.copyTpl(
      this.templatePath(".nycrc"),
      this.destinationPath(".nycrc")
    );
    this.fs.copyTpl(
      this.templatePath("tsconfig.json"),
      this.destinationPath("tsconfig.json")
    );
    this.fs.copyTpl(
      this.templatePath("tslint.json"),
      this.destinationPath("tslint.json")
    );
  }

  _writingEntryFiles() {
    this.fs.copyTpl(
      this.templatePath("module.ts"),
      this.destinationPath("lib", `${this.options.name}.module.ts`),
      {
        moduleName: string.capitalize(this.options.name),
      }
    );
  }

  _writingTestFiles() {
    this.fs.copyTpl(
      this.templatePath("mocha.opts"),
      this.destinationPath("test", "mocha.opts")
    );
  }

  _getDefaultName() {
    const dir = path.basename(process.cwd());
    return NameParser.normalize(dir);
  }

  _getDefaultDescription(name) {
    return `NestJS ${name} module`;
  }
};
