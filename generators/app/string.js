/*
 * Copyright (c) 2019 Juan Manuel Garcia Junco Moreno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Convert the first letter of a string to uppercase
 *
 * @param {string} string String to capitalize
 *
 * @return {string} Capitalized string
 */
function capitalize(string) {
  return string.replace(/^\w/, c => c.toUpperCase());
}

exports.capitalize = capitalize;
