/*
 * Copyright (c) 2019 Juan Manuel Garcia Junco Moreno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
const os = require("os");
const path = require("path");
const util = require("util");
const child = require("child_process");
const fs = require("./fs");

const exec = util.promisify(child.exec);

/**
 * Check if an exacutable exists
 *
 * @param {string} name Name of the executable
 *
 * @return {Promise<boolean>} True if the executable exists
 */
async function executableExists(name) {
  let commandExists = await fs.exists(name);
  const normalizedName = path.basename(path.normalize(name));

  if (!commandExists) {
    const checkCommand =
      os.platform() === "win32"
        ? `where ${normalizedName}`
        : `command -v ${normalizedName}`;

    try {
      await exec(checkCommand);
      commandExists = true;
    } catch (e) {
      commandExists = false;
    }
  }

  return commandExists;
}

exports.executableExists = executableExists;
