/*
 * Copyright (c) 2019 Juan Manuel Garcia Junco Moreno.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
const NameTypes = {
  KEBAB: "kebab",
  SNAKE: "snake",
  LOWER: "lower",
  UPPER: "upper",
};

const NameParser = {
  normalize(name) {
    const type = this._getNameType(name);

    if (!type) {
      throw new Error(`invalid name ${name}`);
    }

    const sep = this._getNameSeparator(type);
    return name
      .split(sep)
      .filter(word => !this._isReservedKeyword(word))
      .map(word => word.toLowerCase())
      .join(sep);
  },

  _getNameType(name) {
    if (/^[A-Za-z]+(-[A-Za-z0-9]+)+$/.test(name)) {
      return NameTypes.KEBAB;
    }

    if (/^[A-Za-z]+(_[A-Za-z0-9]+)+$/.test(name)) {
      return NameTypes.SNAKE;
    }

    if (/^[a-z0-9]+$/.test(name)) {
      return NameTypes.LOWER;
    }

    if (/^[A-Z0-9]+$/.test(name)) {
      return NameTypes.UPPER;
    }

    return null;
  },

  _getNameSeparator(nameType) {
    switch (nameType) {
      case NameTypes.KEBAB:
        return "-";
      case NameTypes.SNAKE:
        return "_";
      default:
        return null;
    }
  },

  _isReservedKeyword(token) {
    const keywords = ["nest", "nestjs"];
    return keywords.indexOf(token.toLowerCase()) >= 0;
  },
};

module.exports = NameParser;
