"use strict";
const path = require("path");
const assert = require("yeoman-assert");
const helpers = require("yeoman-test");

describe("generator-nestjs-module:app", () => {
  beforeAll(() => {
    return helpers
      .run(path.join(__dirname, "../generators/app"))
      .withPrompts({ name: "nest-module", description: "Pure awesome" });
  });

  it("creates files", () => {
    assert.file(["dummyfile.txt"]);
  });
});
