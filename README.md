# generator-nestjs-module
> A Yeoman generator for standalone NestJS modules

:warning:

This is very much a **WIP**

## Installation

First, install [Yeoman](http://yeoman.io) and generator-nestjs-module using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-nestjs-module
```

Then generate your new project:

```bash
yo nestjs-module
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## TODO
- [ ] Write unit tests
- [ ] Use some heuristics to check if the module name includes nestjs
- [ ] Add support for module types (e.g. interceptor, service, all of the above).

## License

GPL-3.0 © [juanelorganelo]()
